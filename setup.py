'''
                                  ESP Health
                        Hypertension Disease Definition
                             Packaging Information
                                  
@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: commonwealth informatics 
@contact: http://esphealth.org
@copyright: (c) 2015 
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import setup
from setuptools import find_packages

setup(
    name = 'esp-plugin-hypertension',
    version = '1.6.0',
    author = 'Bob Zambarano',
    author_email = 'bzambarano@commoninf.com',
    description = 'Hypertension disease definition module for ESP Health application',
    license = 'LGPLv3',
    keywords = 'hypertension algorithm disease surveillance public health epidemiology',
    url = 'http://esphealth.org',
    packages = find_packages(exclude=['ez_setup']),
    install_requires = [
        ],
    entry_points = '''
        [esphealth]
        disease_definitions = hypertension:disease_definitions
        event_heuristics = hypertension:event_heuristics
    '''
    )
