from django.db import connection
from ESP.utils import log
from ESP.settings import DATABASES

#temporary table syntax (TT)
if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc': # ms-sql server
    TT_SYNTX="#"
    TT_PRFX="#"
else: # postgres
    TT_SYNTX="temporary "
    TT_PRFX=""

def generate_clinical_hypertension_cases():
    with connection.cursor() as cursor:
        #end stage renal events
        cursor.execute("""
         select distinct on (patient_id) patient_id, date, id as event_id 
          into """ + TT_SYNTX + """tmp_esr_diag 
          from hef_event e0 
          where name='dx:endstagerenal' 
          order by patient_id, date, id;
          """)
        cursor.execute("""
          select e.id, patient_id, date 
          into """ + TT_SYNTX + """temp_inpat_encs 
          from emr_encounter e 
          join gen_pop_tools.rs_conf_mapping cm on cm.src_value=e.raw_encounter_type 
            and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
          group by e.id, patient_id, date
          """)
        cursor.execute("""
          Select id as event_id, object_id, patient_id, date 
          into """ + TT_SYNTX + """tmp_hypert_diag 
          from hef_event e 
          where not exists (select null 
                            from """ + TT_PRFX + """temp_inpat_encs ie 
                            where ie.id=e.object_id) 
                and e.name = 'dx:hypertension'
            and not exists (select null from """ + TT_PRFX + """tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
          """)
        #anti-hypertensive rx events -- rs_conf_mapping non-inpat only
        cursor.execute("""
          select patient_id, date, id as event_id 
          into """ + TT_SYNTX + """tmp_antihypert_presc 
          from hef_event e 
          where name in ('rx:hydrochlorothiazide','rx:chlorthalidone','rx:indapamide','rx:amlodipine', 'rx:clevidipine',          
            'rx:felodipine','rx:isradipine','rx:nicardipine','rx:nicardipine','rx:nifedipine','rx:nisoldipine','rx:diltiazem',
            'rx:verapamil','rx:atenolol','rx:betaxolol','rx:bisoprolol','rx:carvedilol','rx:labetolol','rx:metoprolol','rx:nadolol',
            'rx:nebivolol','rx:pindolol','rx:propranolol','rx:benazepril','rx:catopril','rx:enalapril','rx:fosinopril','rx:lisinopril',
            'rx:moexipril','rx:perindopril','rx:quinapril','rx:ramipril','rx:trandolapril','rx:candesartan','rx:eprosartan',
            'rx:irbesartan','rx:losartan','rx:olmesartan','rx:telmisartan','rx:valsartan','rx:clonidine','rx:doxazosin','rx:guanfacine',
            'rx:methyldopa','rx:prazosin','rx:erazosin','rx:eplerenone','rx:sprinolactone','rx:aliskiren','rx:hydralazine') 
            and not exists (select null from """ + TT_PRFX + """temp_inpat_encs ie where ie.patient_id=e.patient_id and ie.date=e.date)
            and not exists (select null from """ + TT_PRFX + """tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
          """)
        cursor.execute("create index tmp_antihypert_presc_patid_date_idx on " + TT_PRFX + "tmp_antihypert_presc (patient_id, date)")
        cursor.execute("""
          select patient_id, date, id as event_id 
          into """ + TT_SYNTX + """tmp_highbp 
          from hef_event e where name='enc:highbp' 
            and not exists (select null from """ + TT_PRFX + """temp_inpat_encs ie 
                            where ie.patient_id=e.patient_id and ie.date=e.date)
            and not exists (select null from """ + TT_PRFX + """tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
          """)
        cursor.execute("""
          Select patient_id, date, event_id 
          into """ + TT_SYNTX + """tmp_2highbp 
          from (select patient_id, date, lag(date) over (partition by patient_id order by date) as first_date, event_id 
                from """ + TT_PRFX + """tmp_highbp) t0 where date_part('year', age(date,first_date)) = 0
          """)
        cursor.execute("""
          select patient_id, date, criteria 
          into """ + TT_SYNTX + """tmp_ccases 
          from (select *, row_number() over (partition by patient_id order by date) as rownumber 
                from (select patient_id, min(date) as date, criteria 
                      from (select patient_id, date, 'Criteria #2: Dx hypertension'::varchar(2000) as criteria 
                            from """ + TT_PRFX + """tmp_hypert_diag 
                            union select patient_id, date, 'Criteria #3: Rx for  anithypertensive'::varchar(2000) as criteria 
                            from """ + TT_PRFX + """tmp_antihypert_presc 
                            union select patient_id, date, 'Criteria #1: SBP>=140 or DBP>=90 on 2 or more occasions in a year'::varchar(2000) as criteria 
                            from """ + TT_PRFX + """tmp_2highbp) t0 group by patient_id, criteria) t00) t000 where rownumber=1
          """)
        cursor.execute("""
          insert into nodis_case (patient_id, condition, date, criteria, source, status, notes, reportables, created_timestamp, 
                                  updated_timestamp, sent_timestamp, followup_sent, provider_id, inactive_date, isactive) 
          select t0.patient_id, 'hypertension' as condition, t0.date, t0.criteria as criteria, 
                 'urn:x-esphealth:disease:commoninf:hypertension:sql' as source, 'AR' as status,  null::text as notes, 
                 null::text as reportables, current_timestamp as created_timestamp, current_timestamp as updated_timestamp, 
                 null::timestamp as sent_timestamp, FALSE as followup_sent, 1 as provider_id, null::date as inactive_date, 
                 TRUE as isactive 
          from """ + TT_PRFX + """tmp_ccases t0 
          where not exists (select null from nodis_case c where c.condition='hypertension' and c.patient_id=t0.patient_id)
          """)
        counts = cursor.rowcount
        cursor.execute("select patient_id, id as case_id into " + TT_SYNTX + "tmp_ccase_ids from nodis_case where condition='hypertension'")
        #Here are not-highbp events.
        cursor.execute("""
          select patient_id, date, id as event_id 
          into """ + TT_SYNTX + """tmp_not_highbp 
          from hef_event e where name in ('enc:not_highbp_c','enc:not_highbp_w') 
            and not exists (select null from """ + TT_PRFX + """temp_inpat_encs ie 
                            where ie.patient_id=e.patient_id and ie.date=e.date)
          """)
        #pregnancies
        cursor.execute("""
          select patient_id, date, id as event_id into """ + TT_SYNTX + """tmp_preg from hef_event e 
          where name='dx:pregnancy' and exists (select null from """ + TT_PRFX + """tmp_ccases c where c.patient_id=e.patient_id and c.date<e.date)
          """)
        cursor.execute("""
          select patient_id, min(date) as start_date, max(date) + interval '1 year' as end_date 
          into """ + TT_SYNTX + """tmp_preg_seqs 
          from (select patient_id, date, i, dt_i, 
                       sum(case when dt_i + interval '1 year' >= date then 0 else 1 end) over (partition by patient_id order by i) grp 
                from (select patient_id, date, row_number() over (partition by patient_id order by date) i, 
                             lag(date) over (partition by patient_id order by date) dt_i 
                      from """ + TT_PRFX + """tmp_preg 
                      group by patient_id, date) t00) t0 
          group by patient_id, grp order by patient_id, start_date
          """)
        #here are all the events to use in building the caseactivehistory (some of these are not hef events, such as birthdays)
        cursor.execute("""
         select patient_id, date as case_date, hdate, event_id, content_type_id, etype 
         into """ + TT_SYNTX + """tmp_events 
         from (Select distinct c.patient_id, c.date, d.date as hdate, d.event_id, ct.id as content_type_id, 'hdx' etype 
               from """ + TT_PRFX + """tmp_ccases c 
               join """ + TT_PRFX + """tmp_hypert_diag d on c.patient_id=d.patient_id and d.date between c.date and current_date 
               join django_content_type ct on app_label='hef' and model='event' 
               union Select distinct c.patient_id, c.date, hbp.date as hdate, hbp.event_id, ct.id as content_type_id, 'hbp' etype 
               from """ + TT_PRFX + """tmp_ccases c 
               join """ + TT_PRFX + """tmp_highbp hbp on c.patient_id=hbp.patient_id and hbp.date between c.date and current_date 
               join django_content_type ct on app_label='hef' and model='event' 
               union Select distinct c.patient_id, c.date, nhbp.date as hdate, nhbp.event_id, ct.id as content_type_id, 'cbp' etype 
               from """ + TT_PRFX + """tmp_ccases c 
               join """ + TT_PRFX + """tmp_not_highbp nhbp on c.patient_id=nhbp.patient_id and nhbp.date between c.date and current_date 
               join django_content_type ct on app_label='hef' and model='event' 
               union Select distinct c.patient_id, c.date, esr.date as hdate, esr.event_id, ct.id as content_type_id, 'esr' etype 
               from """ + TT_PRFX + """tmp_ccases c 
               join """ + TT_PRFX + """tmp_esr_diag esr on c.patient_id=esr.patient_id and esr.date > c.date and esr.date <= current_date 
               join django_content_type ct on app_label='hef' and model='event' 
               union Select distinct c.patient_id, c.date, prg.date as hdate, prg.event_id, ct.id as content_type_id, 'prg' etype 
               from """ + TT_PRFX + """tmp_ccases c 
               join """ + TT_PRFX + """tmp_preg prg on c.patient_id=prg.patient_id and prg.date between c.date and current_date 
               join django_content_type ct on app_label='hef' and model='event' 
               union select distinct c.patient_id, c.date, rx.date as hdate, rx.event_id, ct.id as content_type_id, 'rxs' etype 
               from """ + TT_PRFX + """tmp_ccases c 
               join """ + TT_PRFX + """tmp_antihypert_presc rx on c.patient_id=rx.patient_id and rx.date between c.date and current_date 
               join django_content_type ct on app_label='hef' and model='event' ) t0
               """)
        cursor.execute("create index tmp_event_etypes on tmp_events (etype)")
        cursor.execute("analyse tmp_events")
        cursor.execute("""
          insert into nodis_case_events (case_id, event_id) 
          select t0.case_id, t1.event_id 
          from """ + TT_PRFX + """tmp_ccase_ids t0 
          join """ + TT_PRFX + """tmp_events t1 on t1.patient_id=t0.patient_id where  
            not exists (select null from nodis_case_events t00 where t00.case_id=t0.case_id and t00.event_id=t1.event_id)
          group by t0.case_id, t1.event_id
          """)
        #get relevant clinical encounters
        cursor.execute("""
          select patient_id, date 
          into """ + TT_SYNTX + """tmp_clin_enc 
          from gen_pop_tools.clin_enc ce 
          where exists (select null from """ + TT_PRFX + """tmp_ccase_ids ci 
                        where ci.patient_id=ce.patient_id) 
                and not exists (select null from """ + TT_PRFX + """temp_inpat_encs ie 
                                where ie.patient_id=ce.patient_id and ie.date=ce.date)
          group by patient_id, date
          """)
        cursor.execute("CREATE INDEX tmp_clin_enc_pat_date_idx ON tmp_clin_enc USING btree (patient_id, date)")
        cursor.execute("Analyze tmp_clin_enc")
        #identify the reverters
        cursor.execute("""
          select patient_id, rdate, next_date, max(cbp_date) as cbp_date 
          into """ + TT_SYNTX + """tmp_reverters 
          from (select * 
                from (select t0.patient_id, 
                          Case 
                            when t0.date + interval '2 years' >= coalesce(max(t1.date)+interval '2 years',t0.date+interval '2 years') 
                              then t0.date+ interval '2 years' 
                            else max(t1.date)+ interval '2 years' 
                          end as rdate, cbp.date cbp_date, t0.next_date 
                      from (select patient_id, date, coalesce(lead(date) over (partition by patient_id order by date), current_date) as next_date 
                            from """ + TT_PRFX + """tmp_hypert_diag) t0 
                            left join """ + TT_PRFX + """tmp_antihypert_presc t1 on t1.patient_id=t0.patient_id 
                                and t1.date between t0.date and t0.next_date 
                            join """ + TT_PRFX + """tmp_not_highbp cbp on cbp.patient_id= t0.patient_id and cbp.date < t0.next_date 
                            where date_part('year', age(t0.next_date,t0.date)) > 2 
                              and exists (select count(*) 
                                          from """ + TT_PRFX + """tmp_clin_enc ce 
                                          where t0.patient_id=ce.patient_id and ce.date between t0.date and t0.next_date 
                                          having count(*) >= 2) 
                            group by t0.patient_id, t0.date, cbp.date, t0.next_date ) t00 
                where not exists (select null 
                                  from """ + TT_PRFX + """tmp_highbp hbp 
                                  where hbp.patient_id=t00.patient_id and hbp.date between t00.cbp_date and t00.next_date) 
                  and rdate < next_date and cbp_date<rdate) t000 
          group by patient_id, rdate, next_date
          """)
        #get BP events
        cursor.execute("""
          select patient_id, hdate, event_id, content_type_id, etype::varchar(3) etype 
          into """ + TT_SYNTX + """tmp_bp_evnts 
          from """ + TT_PRFX + """tmp_events where etype in ('hbp','cbp')
          """)
        cursor.execute("""
          select patient_id, hdate, event_id, content_type_id, etype::varchar(3) etype 
          into """ + TT_SYNTX + """tmp_bp_exlu_evnts 
          from """ + TT_PRFX + """tmp_bp_evnts  t0 
          where not exists (select null from """ + TT_PRFX + """tmp_preg_seqs t1 
                            where t0.hdate between t1.start_date and t1.end_date and t1.patient_id=t0.patient_id) 
            and not exists (select null from """ + TT_PRFX + """tmp_reverters rvrt 
                            where rvrt.patient_id=t0.patient_id and t0.hdate >= rvrt.rdate and t0.hdate < rvrt.next_date)
          """)
        cursor.execute("""
          select patient_id, hdate, event_id, content_type_id, etype::varchar(3) etype 
          into """ + TT_SYNTX + """tmp_dstat_evnts 
          from """ + TT_PRFX + """tmp_events t0 where etype in ('prg','esr')
          """)
        cursor.execute("""
          select patient_id, case_date, hdate, event_id, content_type_id, etype::varchar(3) etype 
          into """ + TT_SYNTX + """tmp_dxonly_evnts 
          from """ + TT_PRFX + """tmp_events where etype in ('hdx','rxs')
          """)
        cursor.execute("""
          select t0.patient_id, t0.hdate, t0.event_id, t0.content_type_id, t0.etype::varchar(3) etype 
          into """ + TT_SYNTX + """tmp_dx_rvrt 
          from """ + TT_PRFX + """tmp_dxonly_evnts t0 
          join """ + TT_PRFX + """tmp_reverters rvrt on  rvrt.patient_id=t0.patient_id and t0.hdate=rvrt.next_date
          """)
        cursor.execute("""
          select t0.patient_id, t0.hdate, t0.event_id, t0.content_type_id, t0.etype::varchar(3) etype 
          into """ + TT_SYNTX + """tmp_dx_events1 
          from """ + TT_PRFX + """tmp_dx_rvrt t0 
          where exists (select null from """ + TT_PRFX + """tmp_reverters rvrt 
                        where rvrt.patient_id=t0.patient_id and t0.hdate=rvrt.next_date 
                          and not exists (select null from """ + TT_PRFX + """tmp_bp_evnts t1 
                                          where t1.patient_id=rvrt.patient_id and  t1.hdate=rvrt.next_date ))
          """)
        cursor.execute("""
          select t0.patient_id, t0.hdate, t0.event_id, t0.content_type_id, t0.etype::varchar(3) etype 
          into """ + TT_SYNTX + """tmp_dx_events2 
          from """ + TT_PRFX + """tmp_dxonly_evnts t0 
          where t0.hdate=t0.case_date 
            and not exists (select null from """ + TT_PRFX + """tmp_bp_evnts t1 
                            where t1.patient_id=t0.patient_id and t1.hdate=t0.hdate)
          """)
        if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc': # ms-sql server
            cursor.execute("with t000_0 as (select *, row_number() over (partition by patient_id order by patient_id, hdate, etype, event_id) as rownum from (select * from #tmp_bp_exlu_evnts union select * from #tmp_dstat_evnts union select * from #tmp_dx_events1 union select * from #tmp_dx_events2) t0) select * into #t00 from #t000_0 where rownum=1")
            cursor.execute("with t00_0 as (select *, row_number() over (partition by patient_id order by patient_id, hdate, case when etype = 'esr' then 0 when etype='prg' then 1 else 2 end) as rownum from #t00) select * into #tmp_pre_pre_hist from #t00_0 where rownum=1 ")
        else:
            cursor.execute("""
              select distinct on (patient_id, hdate) * 
              into temporary tmp_pre_pre_hist 
              from (select distinct on (patient_id, hdate, etype) * 
                    from (select * from tmp_bp_exlu_evnts 
                          union select * from tmp_dstat_evnts 
                          union select * from tmp_dx_events1 
                          union select * from tmp_dx_events2) t0 
                    order by patient_id, hdate, etype, event_id) t00 
              order by patient_id, hdate, case when etype='esr' then 0 when etype='prg' then 1 else 2 end
            """)
        cursor.execute("""
          select *, case when (etype <> coalesce(prior_type,'start') and etype <> coalesce(next_type,'end')) then 'both' 
                      when etype <> coalesce(prior_type,'start') then 'start' 
                      when etype <> coalesce(next_type,'end') then 'end' end as place, 
                    case when (etype <> coalesce(prior_type,'start')) then 1 else 0 end as starts, 
                    lag(hdate) over (partition by patient_id order by hdate) as prior_date 
          into """ + TT_SYNTX + """tmp_prelim_hist 
          from (select *, lag(etype) over (partition by patient_id order by hdate) as prior_type, 
                  lead(etype) over (partition by patient_id order by hdate) as next_type 
                from """ + TT_PRFX + """tmp_pre_pre_hist ) t00 
          order by patient_id, hdate
          """)
        cursor.execute("""
          select *, sum(starts) over (partition by patient_id order by hdate) as sgroup 
          into """ + TT_SYNTX + """tmp_prelim_hist2 
          from """ + TT_PRFX + """tmp_prelim_hist
          """)
        cursor.execute("""
          select patient_id, hdate, latest_event_date, event_id, content_type_id, etype 
          into """ + TT_SYNTX + """tmp_intermed_hist 
          from (select patient_id, hdate, hdate as latest_event_date, event_id, content_type_id, etype, 
                  min(case when etype ='esr' then hdate end) over (partition by patient_id) as min_esrb86 
                from """ + TT_PRFX + """tmp_prelim_hist2 where place='both' 
                union select t0.patient_id, t0.hdate, t1.hdate as latest_event_date, t1.event_id, t1.content_type_id, t1.etype, 
                  min(case when t0.etype ='esr' then t0.hdate end) over (partition by t0.patient_id) as min_esrb86 
                from """ + TT_PRFX + """tmp_prelim_hist2 t0 
                join """ + TT_PRFX + """tmp_prelim_hist2 t1 on t0.patient_id=t1.patient_id 
                  and t0.sgroup=t1.sgroup and t0.place='start' and t1.place='end') t00 
          where min_esrb86 is null or hdate<min_esrb86
          """)
        cursor.execute("""
          select patient_id, latest_event_date+interval '2 years' as hdate, latest_event_date+interval '2 years' as latest_event_date, 
            null::int as event_id, null::int as content_type_id, 'unk'::varchar(3) as etype 
          into """ + TT_SYNTX + """tmp_unk 
          from (select patient_id, hdate, latest_event_date, lead(hdate) over (partition by patient_id order by hdate) as next_date 
                from """ + TT_PRFX + """tmp_intermed_hist 
                where etype in ('hbp','cbp')) t0 
          where latest_event_date+interval '2 years' <next_date 
            and not exists (select null from """ + TT_PRFX + """tmp_reverters t1 
                            where t1.patient_id=t0.patient_id and t0.latest_event_date+interval '2 years' between t1.rdate and t1.next_date)
          """)
        cursor.execute("""
          select distinct on (patient_id, hdate) patient_id, hdate, latest_event_date, event_id, content_type_id, etype 
          into """ + TT_SYNTX + """tmp_history 
          from (Select * from """ + TT_PRFX + """tmp_intermed_hist 
                union select * from """ + TT_PRFX + """tmp_unk 
                union select * from (select t0.patient_id, t1.rdate as hdate, t1.rdate as latest_event_date, null::int as event_id, 
                                       null::int as content_type_id, 'rvt'::varchar(3) as etype 
                                     from """ + TT_PRFX + """tmp_reverters t1 
                                     join """ + TT_PRFX + """tmp_ccases t0 on t1.patient_id=t0.patient_id) t0) t00
          order by patient_id, hdate, case when etype in ('unk','hdx','rxs') then 3 
                   when etype='hbp' then 1 
                   when etype='cbp' then 2 
                   when etype in ('esr','rvt','prg') then 0 
                   else 4
                 end
          """)
        cursor.execute("""
          delete from nodis_caseactivehistory cah 
          where exists (select null from nodis_case c where c.id=cah.case_id and c.condition='hypertension')
          """)
        ##need to figure out here how to update existing histories.  This currently assumes there are no histories and is rebuilding
        cursor.execute("""
          insert into nodis_caseactivehistory (status, date, change_reason, latest_event_date, object_id, case_id, content_type_id) 
          select case when t0.etype in ('unk','hdx','rxs') then 'UK' 
                   when t0.etype='hbp' then 'U' 
                   when t0.etype='cbp' then 'C' 
                   when t0.etype in ('esr','rvt','prg') then 'D' 
                 end status, t0.hdate, 
                 case when t0.etype in ('unk','hdx','hbp','cbp','rxs') then 'Q' when t0.etype in ('esr','rvt','prg') then 'D' end change_reason,
                 t0.latest_event_date, t0.event_id, t1.case_id, t0.content_type_id 
          from """ + TT_PRFX + """tmp_history t0 
          join """ + TT_PRFX + """tmp_ccase_ids t1 on t1.patient_id=t0.patient_id
          """)
        cursor.execute("""
          update nodis_case set isactive=FALSE, inactive_date=t000.date 
          from (select t0.case_id, t0.date from nodis_caseactivehistory t0 
                join (select t00.case_id, max(t00.date) as maxdate 
                      from nodis_caseactivehistory t00 
                      join """ + TT_PRFX + """tmp_ccase_ids t01 on t00.case_id=t01.case_id 
                      group by t00.case_id) t1 on t1.case_id=t0.case_id and t0.status='D' and t0.date=t1.maxdate 
                join nodis_case t2 on t0.case_id=t2.id and t2.condition='hypertension') t000 
          where nodis_case.id=t000.case_id
          """)

        #will eventually need this code for SQL server version of ESP
        #if DATABASES['default']['ENGINE'] == 'sql_server.pyodbc': # ms-sql server
        #else: # postgres
        
        return counts

